<!DOCTYPE html>
<html>
  <head>
  	<link rel="shortcut icon" href="favicon.ico" />
  	<title>CVD Grid</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="grid.css">
    <script src="JS/gridcontrol.js"></script>
    <script src="//js.leapmotion.com/leap-0.4.2.js"></script>
    <script src="JS/leapnumbers.js"></script>
    <script src="JS/videomotion.js"></script>
  </head>
  <body onload="setTimeout(init_callback, 1000);">
gr
<?php

include("./channel.php");
$MAX_CHANNELS = 50;
$_DEBUG = true;

//TODO: make this from a youtube login or arbitary
$_YOUTUBE_USER = "BBC";

/*
LEGACY
$youtube_playlist = [
						"list" => "PL3XZNMGhpynMKXKycwoOMeCd9Nr8f_ntP",
						"list0" => "PL5A4nPQbUF8DwsgOrHdPGdODnCIMYTjwm",
						"list1" => "PL5A4nPQbUF8DgbYPfZnFTo9U_n2ELwLXi",
						"list2" => "RD02lP077RitNAc",
						"list3" => "UUWzLmNWhgeh3h1j-M-Isy0g",
						"list4" => "ELT0FKkN6hTW4"
					];
*/

$youtube_channel_data = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/users/' . $_YOUTUBE_USER . '/playlists?v=2&alt=json&feature=plcp'));
echo "<div class='grid'>";
$i = 0;
$channels = array();
foreach(array_reverse($youtube_channel_data->feed->entry,true) as $a => $s) { //reverse to put newest first

		if(isset($youtube_channel_data->feed->entry[$a])) {
			$channels[$i] = new channel($youtube_channel_data->feed->entry[$a]->{'yt$playlistId'}->{'$t'}, $i); 
			
			if($channels[$i]->load()){
				echo $channels[$i]->html;
				$i++;
			}
		}

		if($i === $MAX_CHANNELS) { 
			break;
		}
}
if($_DEBUG) {
	echo "</div>";
	echo "<div class='testing'>";
		echo "<div class='test_title'>";
			echo "Test Buttons";
		echo "</div>";

		echo "<div class='test_buttons'>";
		//ikky  test buttons
			echo "<button onClick='changeCard(1)'>next</button>";
			echo "<button onClick='changeCard(2)'>prev</button>";
			echo "<button onClick='changeCard(10)'>up</button>";
			echo "<button onClick='changeCard(20)'>down</button>";
			echo "<button onClick='playSelected()'>play</button>";
		echo "</div>";
	echo "</div>";
}
?>
</body>
</html>