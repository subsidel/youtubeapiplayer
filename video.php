<?php 
header('Content-Type: text/html; charset=utf-8');
$_DEBUG = true;
$vidMode = false;

if(isset($_GET['vidId'])) {
?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="shortcut icon" href="favicon.ico" />

    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="video.css">
        <script src="http://js.leapmotion.com/0.2.0-beta1/leap.min.js"></script>
    <script src="JS/leapnumbers.js"></script>
    <script src="JS/videomotion.js"></script>
  </head>
  <body onload="setTimeout(init_callback, 1000);">
    <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
    <div id="player" class="wrapper center"></div>
    <!-- test buttons -->
    <br>
<?php if($_DEBUG) { ?>
  <div class="testing">
    <div class="test_title">
      Test Buttons
    </div>

    <div class="test_buttons">
         <button onClick="stopVideo()">STOP</button>
        <button onClick="playVideo()">PLAY</button>
    </div>
  </div>
<?php  } ?>

<?php
  $youtube_video_data = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/videos/' . $_GET['vidId'] . '?v=2&alt=jsonc&feature=plcp"></script>'));
  $youtube_username = $youtube_video_data->data->uploader;
  $youtube_user_data = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/users/' . $youtube_username . '?v=2&alt=json&feature=plcp')); 
  $youtube_thumb_url = $youtube_user_data->entry->{'media$thumbnail'}->url;
  $youtube_meta_title = $youtube_video_data->data->title;
  $youtube_meta_data = str_replace("\n",'<br />',htmlspecialchars($youtube_video_data->data->description));
?>

<title><?php echo $youtube_meta_title; ?></title>

<div class ='meta'>
    <img class = 'user-thumb' src = <?php echo $youtube_thumb_url; ?> >
    <div class = 'meta-title'>
      <?php echo $youtube_meta_title; ?>

    </div>
    <div class = 'meta-data'>
      <?php echo $youtube_meta_data; ?>
    </div>
</div>

<?php
echo '
    <script>
    var playbackMode = true;
    var defaultHeight = 500; 
    var defaultWidth = 900;
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement("script");

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName("script")[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player("player", {
          height: 500,
          width: 900,

          videoId:"' . $_GET['vidId'] . '",
          playerVars: { 
            autoplay: -1,
            controls: 1 },

          events: {
            "onReady": onPlayerReady,
            "onStateChange": onPlayerStateChange
          }
        });
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        event.target.playVideo();
      }

      // 5. The API calls this function when the player\'s state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          //player.mute();
          done = true;
        }
      }
      var playlistIndex = 0;
      function stopVideo() {
        player.pauseVideo();
      }

      function playVideo() {
        player.playVideo();
      }

      function togglePlaying() {

        if(player.getPlayerState() === 1) {
          player.pauseVideo();
        } else if(player.getPlayerState() === 2) {
          player.playVideo();
        }
      }

      function skipTo(skipAmount,direction) {
        var timeTo = player.getCurrentTime();

        if(direction == numbers.CLOCKWISE) {
          timeTo += skipAmount;
          console.log("add");
        } else {
          timeTo -=skipAmount;
          console.log("take");
        }
        player.seekTo(timeTo,true);
      }

      function volumeControl(direction) {
        var volumeTo = player.getVolume();

        if(direction == numbers.UP) {
          volumeTo += 10;
        } else {
          volumeTo -= 10;
        }
        player.setVolume(volumeTo);
      }

      function setVolume(amount) { 
        var volumeTo = player.getVolume() + amount;
        console.log(volumeTo);
        player.setVolume(volumeTo);

      }
      function playerSize(direction) {

        if(direction == numbers.FORWARD) {
          player.setSize(window.innerWidth - 10, window.innerHeight - 10);
        } else {
          player.setSize(defaultWidth, defaultHeight)
        }
        player.setPlaybackQuality(player.getAvailableQualityLevels()[0]);
      }

    </script>';
}
?>

  </body>
</html>