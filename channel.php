<?php 
class channel{

	public $html;
	private $youtube_link;
	private $channel_number;
	private $show_title = true;
	private $LOW_QUALITY_THUMB = 0;
	private $MED_QUALITY_THUMB = 1;
	private $HIGH_QUALITY_THUMB = 2;
	
	
	function __construct($youtube_link, $channel_number){

		$this->youtube_link = $youtube_link;
		$this->channel_number = $channel_number;	
		$this->html = "";

	}


	public function load(){

		$listMode = true;
		  $cont = json_decode(file_get_contents('http://gdata.youtube.com/feeds/api/playlists/' . $this->youtube_link . '/?v=2&alt=json&feature=plcp')); 
		  if(!isset($cont->feed->entry)){ return false;}
		  $feed = $cont->feed->entry; 

		  if(count($feed)) {
		    $max_count = 0;
		    $this->html .= '<div class="channel '.$this->channel_number.'">';
		    $this->html .= '<div class="video-title">';
		    	
		    	if($this->show_title){
		    		$this->html .= ($cont->feed->title->{'$t'});		    		
		    	}

		    $this->html .= '</div>';
		    $class = "card selected";
		    foreach($feed as $item) { // youtube start
		    	if(isset($item->{'media$group'}->{'media$thumbnail'})){
					if($max_count === 0 && $this->channel_number === 0) {
						$class = "card selected";	
					} else {
						$class = "card";	
					}
					$this->html .= "<div class='$class' id='channel" . $this->channel_number . "card$max_count'>";
					$this->html .= "<img class='thumbnail ' title='$max_count' id='" . $item->{'media$group'}->{'yt$videoid'}->{'$t'} . "'' src='" . $item->{'media$group'}->{'media$thumbnail'}[$this->HIGH_QUALITY_THUMB]->{'url'} . "''>";
					$this->html .= "</div>";
					$max_count++;
				}
			}
		    $this->html .= '</div>';
		  } // youtube end 
		  return true;
		}



}

?>