function leap_numbers() {
	this.TOUCH_PLAIN = 0;
	this.VOLUME_MOD = 20;
	this.VIDEO_STOPPED = 0;
	this.VIDEO_PLAYING = 1;
	this.VIDEO_PAUSED = 2;
	this.CLOCKWISE = 1;
	this.ANTICLOCKWISE = 2;
	this.LEFT_DOWN_FORWARD = 111;
	this.LEFT_DOWN_BACK = 211;
	this.LEFT_UP_FORWARD = 121;
	this.LEFT_UP_BACK = 221;
	this.RIGHT_DOWN_FORWARD = 112;
	this.RIGHT_DOWN_BACK = 212;
	this.RIGHT_UP_FORWARD = 122;
	this.RIGHT_UP_BACK = 222;
	this.LEFT_DOWN = 11;
	this.LEFT_UP = 21;
	this.LEFT_FORWARD = 101;
	this.LEFT_BACK = 201;
	this.RIGHT_DOWN = 12;
	this.RIGHT_UP = 22;
	this.RIGHT_FORWARD = 102;
	this.RIGHT_BACK = 202;
	this.LEFT = 1;
	this.RIGHT = 2;
	this.DOWN = 10;
	this.UP = 20;
	this.FORWARD = 100;
	this.BACK = 200;
	this.PI = 3.14159;
	this.clockwiseness = 1;
	this.canSeeHand = false;
	this.swipeDirections = 0;
	this.circles = 1;
	this.circleDirection = 0;
	this.previousClockwiseness = "";
	this.beginSwipe = false;
	this.endSwipe = false;
	this.startX;
	this.endX;
	this.startY;
	this.endY;
	this.startZ;
	this.endZ;
	this.frm;
	this.xSensitivity = 50;
	this.ySensitivity = 170;
	this.zSensitivity = 100;
	this.horizontalScreenSize = 0;
	this.verticalScreenSize = 0;
	this.windowHeight = 480;
	this.windowWidth = 640;
	this.swiped_direction = 0;
	this.leftFingerCount = 0;

	this.xDifference = function() {
		return this.startX - this.endX;
	}
	this.yDifference = function() {
		return this.startY - this.endY;
	}
	this.zDifference = function() {
		return this.startZ - this.endZ;
	}
};