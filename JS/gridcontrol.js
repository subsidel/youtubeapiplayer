var channelPositions = Array();
var playbackMode = false;
var currentCard = {
					channel: 0,
					card: 0,
				};

var checkValidCard = function() {

	if(currentCard.channel < 0) {
		currentCard.channel = document.getElementsByClassName("channel").length - 1;
	}

	if(currentCard.card < 0) {
		currentCard.card = document.getElementsByClassName("channel " + currentCard.channel)[0].children.length - 2; //-1 for natural to array, -1 for last
	}

	if(currentCard.channel >= document.getElementsByClassName("channel").length) {
		currentCard.channel = 0;
	}

	if(currentCard.card >= document.getElementsByClassName("channel " + currentCard.channel)[0].children.length - 1) {
		currentCard.card = 0;
	}
}

var moveCards = function() {

	//cover your childrens eyes

	for(var i = 0; i < currentCard.channel; i++) {
    	document.getElementsByClassName(i)[0].className += " above";
    }
    for(var i = currentCard.channel; i < document.getElementsByClassName("channel").length; i++) {
    	document.getElementsByClassName(i)[0].className = document.getElementsByClassName(i)[0].className.replace(/(?:^|\s)above(?!\S)/g , '');
    }


	for(var i = 0; i < currentCard.card; i++) {
    	document.getElementsByClassName("channel " + currentCard.channel)[0].querySelectorAll('div:nth-child(' + (i + 2) + ')')[0].className += " behind";
    }
    for(var i = currentCard.card; i < document.getElementsByClassName("channel " + currentCard.channel)[0].children.length - 1; i++) {
    	document.getElementsByClassName("channel " + currentCard.channel)[0].querySelectorAll('div:nth-child(' + (i + 2) + ')')[0].className =
    	 document.getElementsByClassName("channel " + currentCard.channel)[0].querySelectorAll('div:nth-child(' + (i + 2) + ')')[0].className.replace(/(?:^|\s)behind(?!\S)/g , '');
    }
}

var changeCard = function(direction) {

	channelPositions[currentCard.channel] = Array();
	channelPositions[currentCard.channel]["channel"] = currentCard.channel;
	channelPositions[currentCard.channel]["card"] = currentCard.card;
	var currentId = "channel" + currentCard.channel + "card" + currentCard.card;

	document.getElementById(currentId).className = document.getElementById(currentId).className.replace(/(?:^|\s)selected(?!\S)/g , '');

	switch(direction) {
		case numbers.RIGHT:
			currentCard.card -= 1;
			break;
		case numbers.LEFT:
			currentCard.card += 1;
			break;
		case numbers.DOWN:
			currentCard.channel -= 1;
			break;
		case numbers.UP:
			currentCard.channel += 1;
			break;
	}
	if(direction == numbers.UP || direction == numbers.DOWN){
		if(typeof (channelPositions[currentCard.channel]) != "undefined") {
			currentCard.card = channelPositions[currentCard.channel]["card"];
		}
	}
	
	
	checkValidCard();
	
	console.log(currentCard);
	var currentId = "channel" + currentCard.channel + "card" + currentCard.card;
    document.getElementById(currentId).className += " selected";

    moveCards();

}

var playSelected = function() {
	selectedVideoId = document.getElementsByClassName("selected")[0].firstChild.id;
	origin = window.location.origin;
	window.location.href = origin + "/dissertation/video.php?vidId=" + selectedVideoId;
}