var frame;
var numbers = new leap_numbers();
var gesture_lock = false;
var swipe_lock = false;
var properly_initialised = false;

var doSwipe = function(direction, playback) {

    if(!swipe_lock) {//prevent misscalling & double calls
console.log(direction);
        switch(direction) {
        case numbers.LEFT_UP:
        case numbers.LEFT_BACK:
        case numbers.LEFT_FORWARD:
        case numbers.LEFT:

            if(playback) {

            } else {
                changeCard(numbers.LEFT);
            }
            break;
        case numbers.RIGHT_UP:
        case numbers.RIGHT_BACK:
        case numbers.RIGHT_FORWARD:
        case numbers.RIGHT:

            if(playback) {
                window.history.back();
            } else {
                changeCard(numbers.RIGHT);
            }
            break;
        case numbers.UP:

            if(playback) {
                volumeControl(direction);
            } else {
                changeCard(direction);
            }
            break;
        case numbers.DOWN:

            if(playback) {
                volumeControl(direction);
            } else {
                changeCard(direction);
            }
            break;
        }
    }
}

function lockSwipe() {
    swipe_lock = true;
    setTimeout(function(){swipe_lock = false;}, 500);
}

function onScreenTap(gesture, playback) {

    if(!playback) {
        playSelected();
    } else {
        togglePlaying();
    }    
};

function onKeyTap(gesture, playback) {
    
    if(!swipe_lock){
    
        if(!playback) {
             console.log("tap");
            changeCard(numbers.DOWN);
        } else {
            volumeControl(numbers.down);
        }
        lockSwipe();
    }
};

var initial_fingr_y = 0;
var initial_fingr_z = 0;
function doubleFinger(finger, frame) {

    var interactionBox = frame.interactionBox;
    var normalisedFinger = interactionBox.normalizePoint(finger.tipPosition, true);
    var fngr_y = normalisedFinger[1];
    var fngr_z = finger.tipPosition[2];
    var touching = (fngr_z < numbers.TOUCH_PLAIN);

    if(!touching) {
        //console.log("not touch");
        initial_fingr_y = fngr_y;
    } else {
        //console.log("touch");
        amount = (fngr_y - initial_fingr_y) * numbers.VOLUME_MOD;
        //console.log(amount);
        setVolume(amount);
       // initial_fingr_y = fngr_y;
    } 

}

function onCircle(gesture, playback) {

    if(playback) {
    // First get our position using the leapToScene Function
        var pos = leapToScene(gesture.center);

        // Assigning the radius
        var r = gesture.radius;
        var circle = gesture;
        
        if (circle.normal[2] <= 0) {
            numbers.clockwiseness = numbers.CLOCKWISE;
            //clockwise stuff
        } else {
            numbers.clockwiseness = numbers.ANTICLOCKWISE;
            //counterclockwise stuff
        }
        console.log(numbers.clockwiseness);

        // Calculate angle swept since last frame
        var sweptAngle = 0;
        
        if (circle.state != gesture.STATE_START) {
            var previousUpdate = circle;
            sweptAngle = (circle.progress - previousUpdate.progress * 2 * numbers.PI);
        }

        if(circle.progress < 1 || numbers.previousClockwiseness != numbers.clockwiseness) {
            numbers.circles = 1;
        }

        if(circle.progress > numbers.circles) {
            numbers.circles++;
            
            if(numbers.circles < 4) {
                skipTo(1, numbers.clockwiseness);
            } else if(numbers.circles >= 4) {
                skipTo(5, numbers.clockwiseness);
            }
            
        }
        console.log(numbers.circles);
        numbers.previousClockwiseness = numbers.clockwiseness;
    }  
}

function handleSwipe (swipe, playback) {
    
    if(!swipe_lock){
        var startFrameID;

        if(swipe.state == "start" && !numbers.beginSwipe) {
            numbers.beginSwipe = true;
            numbers.endSwipe = false;
            numbers.startX = swipe.startPosition[0];
            numbers.startY = swipe.startPosition[1];
            numbers.startZ = swipe.startPosition[2];
        } else if(swipe.state == "stop" && !numbers.endSwipe) {
            numbers.endX = swipe.position[0];
            numbers.endY = swipe.position[1];
            numbers.endZ = swipe.position[2];
        }

        var xDirection = "";
        var yDirection = "";
        var zDirection = "";
        numbers.swipeDirections = 0;
        
        if(Math.abs(numbers.yDifference()) > numbers.ySensitivity) {
            var numDir;
            
            if(numbers.yDifference() > 0) {

                yDirection = "down";
                console.log("down-swipe");
                numbers.swipeDirections +=  numbers.DOWN;
            } else {
                yDirection = "up";
                console.log("up-swipe");
                numbers.swipeDirections += numbers.UP;
            }
        }

        if(Math.abs(numbers.xDifference()) > numbers.xSensitivity) { 
        
            if(numbers.xDifference() > 0 ) {
                xDirection = "left";
                numbers.swipeDirections += numbers.LEFT;
                console.log("left-swipe");
            } else {
                xDirection = "right";
                numbers.swipeDirections += numbers.RIGHT;
                console.log("right-swipe");
            }
        }

        if(Math.abs(numbers.zDifference()) > numbers.zSensitivity) {

            if(numbers.zDifference() > 0) {
                zDirection = "forward ";
                numbers.swipeDirections += numbers.FORWARD;
                //playSelected();
            } else {
                zDirection = "back ";
                numbers.swipeDirections += numbers.BACK;
            }
        }

        if(swipe.state == "stop" && !numbers.endSwipe) {
            endSwipe = true;
            beginSwipe = false;
            swipeDirections = xDirection + yDirection + zDirection;
            console.log(swipeDirections);
        }

        if(numbers.swipeDirections !== 0) {
            doSwipe(numbers.swipeDirections, playback);
            lockSwipe();
        }
        numbers.swipeDirections = 0;
        //reset positions
        numbers.startX = 0;
        numbers.endX = 0;
        numbers.startY = 0;
        numbers.endY = 0;
        numbers.startZ = 0;
        numbers.endZ = 0;
    }

}



 function leapToScene(leapPos) {

      var iBox = frame.interactionBox;

      var left = iBox.center[0] - iBox.size[0]/2;
      var top = iBox.center[1] + iBox.size[1]/2;

      var x = leapPos[0] - left;
      var y = leapPos[1] - top;

      x /= iBox.size[0];
      y /= iBox.size[1];

      return [x , - y];

    }

var init_callback = function() {
    setTimeout(init, 500);
}

var init = function() {
    console.log("initialising");

    var controller = new Leap.Controller({enableGestures: true});
    //controller.connect();


    controller.on('frame',function(data) {
        if(data.valid) {
            frame = data;
        }
    });

    controller.on('gesture', function (gesture) {
        if(!gesture_lock){
            var type = gesture.type;
            switch(type) {
            case "circle":
                onCircle(gesture, playbackMode);
                break;
            case "swipe":
                handleSwipe(gesture, playbackMode);
                break;
            case "screenTap":
                onScreenTap(gesture, playbackMode);
                break;
            case "keyTap":
                onKeyTap(gesture, playbackMode);
                break;
            }
        }
    });

    controller.connect();

    if(!properly_initialised) {
        init_callback();
        properly_initialised = true;
    }

    Leap.loop(function(frame) {

        if(playbackMode){
             if(frame.hands.length == 1) {
                if(frame.hands[0].fingers.length == 2) {
                    gesture_lock = true;
                    doubleFinger(frame.hands[0].fingers[0], frame);
                } else {
                    gesture_lock = false;
                }
            } else if(frame.hands.length == 2) {
                console.log("2");
                gesture_lock = true;
                if((frame.hands[0].fingers.length == 5 && frame.hands[1].fingers.length == 0) ||
                     (frame.hands[0].fingers.length == 5 && frame.hands[1].fingers.length == 0)) {
                    playerSize(numbers.BACK);
                }else if(frame.hands[0].fingers.length == 5 && frame.hands[1].fingers.length == 5) {
                    playerSize(numbers.FORWARD);
                }

            }else {
                gesture_lock = false;
            }

        }
    });

};